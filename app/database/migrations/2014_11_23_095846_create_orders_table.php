<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('orders', function (Blueprint $table) {
			// 编号
			$table->increments('id');
			// 订单号
			$table->string('number', 60);
			// 订单状态
			$table->string('status', 20);
			// 子项统计
			$table->integer('items_total');
			// 调整项
			$table->string('adjustments');
			// 调整金额,以分为单位
			$table->integer('adjustments_total');
			// 订单金额,以分为单位
			$table->integer('total');
			// 已支付金额,以分为单位
			$table->integer('paid_total');
			// 用户id
			$table->integer('user_id');
			// 联系人
			$table->string('contact');
			// 联系电话
			$table->string('mobile');
			// 联系地址
			$table->string('address');
			// 订单完成时间
			$table->timestamp('completed_at');
			// 订单删除时间
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('orders');
	}

}
