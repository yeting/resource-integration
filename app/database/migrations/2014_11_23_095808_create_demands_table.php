<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDemandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('demands', function (Blueprint $table) {
			// 编号
			$table->increments('id');
			// 名称
			$table->string('name');
			// 图片
			$table->string('images');
			// 描述
			$table->text('description');
			// 有效起始时间
			$table->timestamp('available_on');
			// 删除时间
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('demands');
	}

}
