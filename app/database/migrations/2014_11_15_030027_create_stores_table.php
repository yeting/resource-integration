<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('stores', function (Blueprint $table) {
			// 席位编号
			$table->increments('id');
			// 席位名称
			$table->string('name');
			// 席位详情介绍
			$table->text('description');
			// 席位图片URL
			$table->string('images');
			// 席位公司信息
			$table->string('company');
			// TODO 11/23 yt 席位公司信息
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('stores');
	}

}
