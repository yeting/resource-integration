<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('order_logs', function (Blueprint $table) {
			$table->increments('id');
			// 操作人 用户 or 系统 0
			$table->integer('user_id');
			// 操作内容
			$table->string('log');
			// 操作时间
			$table->timestamp('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('order_logs');
	}

}
