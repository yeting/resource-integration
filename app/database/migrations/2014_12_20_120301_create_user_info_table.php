<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/**
		 * 用户信息表
		 */
		Schema::create('user_info', function(Blueprint $table)
		{

			$table->increments('id');
			// 用户编号
			$table->integer('user_id');
			// 性别
			$table->string('gender',20);
			// 生日
			$table->timestamp('birthday');
			// 常用手机
			$table->string('mobile',20);
			// 联系住址
			$table->string('address');
			// 所在单位
			$table->string('company');
			// 职位
			$table->string('position');
			// 单位地址
			$table->string('company_address');

			$table->timestamps();
			// index 
			$table->index('user_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_info');
	}

}
