<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('order_items', function (Blueprint $table) {
			// 编号
			$table->increments('id');
			// 订单编号
			$table->integer('order_id');
			// 单位价格
			$table->integer('unit_price');
			// 数量
			$table->integer('quantity');
			// 调整项
			$table->string('adjustments');
			// 调整项
			$table->integer('adjustments_total');
			// 总价格
			$table->integer('total');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('order_items');
	}

}
