<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryableTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categoryables', function(Blueprint $table)
		{
			// 分类关联表
			$table->integer('category_id');
			// 关联分类的实体主键
			$table->integer('categoryable_id');
			// 管类分类的实体类型
			$table->string('categoryable_type');
			// $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categoryable');
	}

}
