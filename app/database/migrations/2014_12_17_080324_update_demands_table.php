<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateDemandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('demands', function (Blueprint $table) {
			$table->integer('store_id');
			$table->integer('price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('demands', function (Blueprint $table) {
			$table->dropColumn('store_id');
			$table->dropColumn('price');
		});
	}

}
