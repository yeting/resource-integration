<?php

// Composer: "fzaninotto/faker": "v1.3.0"

class UserTableSeeder extends Seeder {

	public function run() {



		$group = Sentry::createGroup(array(
			'name'        => 'Admin',
			'permissions' => array(
				'admin' => 1,
				'users' => 1,
			),
		));

		$user = Sentry::createUser(array(
			'username'  => 'admin',
			'password'  => 'admin',
			'activated' => true,
		));
		// Find the group using the group id
		$adminGroup = Sentry::findGroupById(1);

		// Assign the group to the user
		$user->addGroup($adminGroup);

		$user = Sentry::createUser(array(
			'username'     => 'test',
			'password'  => 'test',
			'activated' => true,
		));


		// 导入席卡用户
		// Excel::selectSheets('Sheet1')->load('app/database/seeds/InitUser.xls', function($reader) {
		//     $reader->each(function($row) {

		//     	$user = Sentry::createUser(array(
		// 			'username'  => '000'.$row->username,
		// 			'password'  => $row->password,
		// 			'activated' => true,
		//     	));

		//     	$store = Store::create([
		// 			'store_no' => $row->store_no,
		//     	]);

		//     	$store->users()->attach($user->id);
		//     });

		// });
		Excel::filter('chunk')->load('app/database/seeds/InitUser.csv')->chunk(50, function($results)
		{
		        foreach($results as $row)
		        {
                	$user = Sentry::createUser(array(
            			'username'  => '000'.$row->username,
            			'password'  => $row->password,
            			'activated' => true,
                	));

                	$store = Store::create([
            			'store_no' => $row->store_no,
                	]);

                	$store->users()->attach($user->id);						
		              
		        }
		});
		
		// $faker = Faker::create();

		// foreach(range(1, 10) as $index)
		// {
		// 	User::create([

		// 	]);
		// }
	}

}