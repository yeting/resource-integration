<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrderItemTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		foreach (range(1, 10) as $index) {
			OrderItem::create([
				'order_id' => $faker->randomDigitNotNull,
				'unit_price' => $faker->numberBetween($min = 10000, $max = 90000),
				'quantity' => $faker->randomDigitNotNull,
				'adjustments_total' => $faker->numberBetween($min = 10000, $max = 90000),
				'total' => $faker->numberBetween($min = 100000, $max = 900000),

			]);
		}
	}

}