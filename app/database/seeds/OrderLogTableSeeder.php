<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrderLogTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		foreach (range(1, 10) as $index) {
			OrderLog::create([
				'user_id' => $faker->numberBetween($min = 0, $max = 10),
				'log' => $faker->sentence($nbWords = 6),
				'updated_at' => $faker->dateTime($max = 'now'),
			]);
		}
	}

}