<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		foreach (range(1, 10) as $index) {
			Product::create([
				'name' => $faker->sentence($nbWords = 6),
				'description' => $faker->paragraph($nbSentences = 5),
				'images' => $faker->imageUrl($width = 640, $height = 480),
				'available_on' => $faker->dateTime($max = 'now'),
			]);
		}
	}

}