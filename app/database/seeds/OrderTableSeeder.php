<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrderTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		foreach (range(1, 10) as $index) {
			Order::create([
				'number' => $faker->ean8,
				'status' => $faker->randomElement($array = array('pending', 'confirmed', 'shipped', 'abandoned', 'cancelled', 'returned')),
				'items_total' => $faker->randomDigitNotNull,
				'adjustments_total' => $faker->numberBetween($min = 10000, $max = 100000),
				'total' => $faker->numberBetween($min = 500000, $max = 1000000),
				'paid_total' => $faker->numberBetween($min = 50000, $max = 100000),
				'user_id' => $faker->randomDigitNotNull,
				'contact' => $faker->name($gender = null|'male'|'female'),
				'mobile' => $faker->phoneNumber,
				'address' => $faker->address,
				'completed_at' => $faker->dateTimeBetween($startDate = '-5 days', $endDate = '30 days'),

			]);
		}
	}

}