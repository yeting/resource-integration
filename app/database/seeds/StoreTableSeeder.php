<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class StoreTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		Eloquent::unguard();

		foreach (range(1, 30) as $index) {
			$store = Store::create([
				'name'        => $faker->sentence($nbWords = 4),
				'description' => $faker->paragraph($nbSentences = 5),
				'images'      => $faker->imageUrl($width = 640, $height = 640),
				'company'     => $faker->company,
			]);

			foreach (range(1, 30) as $value) {
				// 创建席位产品
				$store->products()->save(new Product([
					'name'         => $faker->sentence($nbWords = 4),
					'description'  => $faker->paragraph($nbSentences = 5),
					'images'       => $faker->imageUrl($width = 640, $height = 640),
					'price'        => $faker->numberBetween($min = 10000, $max = 900000),
					'available_on' => $faker->dateTime($max = 'now'),
				]));
				// 创建席位服务
				$store->services()->save(new Service([
					'name'         => $faker->sentence($nbWords = 4),
					'description'  => $faker->paragraph($nbSentences = 5),
					'images'       => $faker->imageUrl($width = 640, $height = 640),
					'price'        => $faker->numberBetween($min = 10000, $max = 900000),
					'available_on' => $faker->dateTime($max = 'now'),
				]));
				// 创建席位需求
				$store->demands()->save(new Demand([
					'name'         => $faker->sentence($nbWords = 4),
					'description'  => $faker->paragraph($nbSentences = 5),
					'images'       => $faker->imageUrl($width = 640, $height = 640),
					'price'        => $faker->numberBetween($min = 10000, $max = 900000),
					'available_on' => $faker->dateTime($max = 'now'),
				]));
			}

		}
	}

}