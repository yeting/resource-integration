<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoryTableSeeder extends Seeder {

	public function run() {
		$faker = Faker::create();

		foreach (range(1, 10) as $index) {
			Category::create([
				'name' => $faker->name,
				'description' => $faker->text,
				'pid' => 0,
				'type' => $faker->randomElement($array = array('service', 'store', 'product')),

			]);
		}
	}

}