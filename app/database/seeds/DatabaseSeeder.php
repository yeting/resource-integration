<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Eloquent::unguard();
		// $this->call('StoreTableSeeder');
		$this->call('CategoryTableSeeder');
		$this->call('DemandTableSeeder');
		$this->call('OrderTableSeeder');
		$this->call('OrderItemTableSeeder');
		$this->call('OrderLogTableSeeder');
		// $this->call('ProductTableSeeder');
		// $this->call('ServiceTableSeeder');
		// $this->call('UserTableSeeder');
	}

}
