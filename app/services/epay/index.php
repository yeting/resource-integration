<?php
/* *
 * 功能：e支付即时到账交易接口接口调试入口页面
 * 版本：1.0
 * 日期：2013-12-14
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
	<title>e支付即时到账交易接口接口</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
*{
	margin:0;
	padding:0;
}
ul,ol{
	list-style:none;
}
.title{
    color: #ADADAD;
    font-size: 14px;
    font-weight: bold;
    padding: 8px 16px 5px 10px;
}
.hidden{
	display:none;
}

.new-btn-login-sp{
	border:1px solid #D74C00;
	padding:1px;
	display:inline-block;
}

.new-btn-login{
    background-color: transparent;
    background-image: url("images/new-btn-fixed.png");
    border: medium none;
}
.new-btn-login{
    background-position: 0 -198px;
    width: 82px;
	color: #FFFFFF;
    font-weight: bold;
    height: 28px;
    line-height: 28px;
    padding: 0 10px 3px;
}
.new-btn-login:hover{
	background-position: 0 -167px;
	width: 82px;
	color: #FFFFFF;
    font-weight: bold;
    height: 28px;
    line-height: 28px;
    padding: 0 10px 3px;
}
.bank-list{
	overflow:hidden;
	margin-top:5px;
}
.bank-list li{
	float:left;
	width:153px;
	margin-bottom:5px;
}

#main{
	width:750px;
	margin:0 auto;
	font-size:14px;
	font-family:'宋体';
}
#logo{
	background-color: transparent;
    background-image: url("images/new-btn-fixed.png");
    border: medium none;
	background-position:0 0;
	width:166px;
	height:35px;
    float:left;
}
.red-star{
	color:#f00;
	width:10px;
	display:inline-block;
}
.null-star{
	color:#fff;
}
.content{
	margin-top:5px;
}

.content dt{
	width:160px;
	display:inline-block;
	text-align:right;
	float:left;
	
}
.content dd{
	margin-left:100px;
	margin-bottom:5px;
}
#foot{
	margin-top:10px;
}
.foot-ul li {
	text-align:center;
}
.note-help {
    color: #999999;
    font-size: 12px;
    line-height: 130%;
    padding-left: 3px;
}

.cashier-nav {
    font-size: 14px;
    margin: 15px 0 10px;
    text-align: left;
    height:30px;
    border-bottom:solid 2px #CFD2D7;
}
.cashier-nav ol li {
    float: left;
}
.cashier-nav li.current {
    color: #AB4400;
    font-weight: bold;
}
.cashier-nav li.last {
    clear:right;
}
.epay_link {
    text-align:right;
}
.epay_link a:link{
    text-decoration:none;
    color:#8D8D8D;
}
.epay_link a:visited{
    text-decoration:none;
    color:#8D8D8D;
}
</style>
</head>
<body text=#000000 bgColor=#ffffff leftMargin=0 topMargin=4>
	<div id="main">
        <div class="cashier-nav">
            <ol>
				<li class="current">1、生成订单 →</li>
				<li>2、支付订单 →</li>
				<li class="last">3、支付完成</li>
            </ol>
        </div>
        <form name=epayment action=epayapi.php method=post target="_blank">
            <div id="body" style="clear:left">
                <dl class="content">
                	
                		<dt>接口名称：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="service" value="create_direct_pay_by_user"/>
                        <span>必填</span>
                    </dd>
                	
                		<dt>合作商ID：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="partner_id" value="1002201305272380"/>
                        <span>必填</span>
                    </dd>
                	
                		<dt>编码方式：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="input_charset" value="utf-8"/>
                        <span>必填，固定</span>
                    </dd>
                    
                    <dt>签名类型：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="sign_type" value="MD5"/>
                        <span>必填，固定</span>
                    </dd>
                    
                    <dt>外部订单号：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="out_trade_no" value="1312111101172576702"/>
                        <span>必填</span>
                    </dd>
                    
                    <dt>交易金额：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="amount" value="0.01"/>
                        <span>必填，单位为元</span>
                    </dd>
                    
                    <dt>支付方式：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="payMethod" value="bankPay"/>
                        <span>必填</span>
                    </dd>
                	
                	
                    <dt>卖家账号：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="seller_email" value="cmbc188@163.com"/>
                        <span>必填</span>
                    </dd>
                    
                    <dt>通知地址：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="notify_url" value="http://epay.cmbc.com.cn/payment/CmbcPayNotify.jsp"/>
                        <span>必须以http或https开头</span>
                    </dd>
                    
                    <dt>交易标题：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="subject" value="demo subject"/>
                        <span>必填，最大255字节</span>
                    </dd>
                    
                    <dt>买家账号：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="buyer_email" value=""/>
                        <span></span>
                    </dd>
                    
                    <dt>交易详细内容：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="body" value="详细内容"/>
                        <span>必填，中文编码为UTF-8。最大400字节</span>
                    </dd>
                    
                    <dt>商品展示网址：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="show_url" value=""/>
                        <span></span>
                    </dd>
                    
                    <dt>默认银行：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="default_bank" value=""/>
                        <span></span>
                    </dd>
                    
                    <dt>跳转地址：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="return_url" value="http://epay.cmbc.com.cn"/>
                        <span>必填，必须以http或https开头</span>
                    </dd>
                    
                    <dt>分润账号：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="royalty_parameters" value=""/>
                        <span></span>
                    </dd>
                    
                    <dt>签约密钥：</dt>
                    <dd>
                        <span class="null-star">*</span>
                        <input size="30" name="signkey" value="d1738053d38616dc27d72a2642a1c0bb"/>
                        <span>必填</span>
                    </dd>
					<dt></dt>
                    <dd>
                        <span class="new-btn-login-sp">
                            <button class="new-btn-login" type="submit" style="text-align:center;">确 认</button>
                        </span>
                    </dd>
                </dl>
            </div>
		</form>
        <div id="foot">
			<ul class="foot-ul">
				<li><font class="note-help">如果您点击“确认”按钮，即表示您同意该次的执行操作。 </font></li>
				<li>
					民生e支付版权所有 2013-2015 epay.cmbc.com.cn 
				</li>
			</ul>
		</div>
	</div>
</body>
</html>