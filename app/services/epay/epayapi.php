<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>e支付即时到账交易接口接口</title>
</head>
<?php
/* *
 * 功能：即时到账交易接口接入页
 * 版本：1.0
 * 修改日期：2013-12-14
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究e支付接口使用，只是提供一个参考。

 */

require_once("lib/epay_submit.class.php");

/**************************请求参数**************************/

				$service = $_POST['service'];//接口名称
				$partner_id = $_POST['partner_id'];//合作商ID
				$input_charset = $_POST['input_charset'];//编码方式
				$sign_type = $_POST['sign_type'];//签名类型
				$out_trade_no = $_POST['out_trade_no'];//外部订单号
				$amount = $_POST['amount'];//交易金额
				$payMethod = $_POST['payMethod'];//支付方式
				$seller_email = $_POST['seller_email'];//卖家账号
				$notify_url = $_POST['notify_url'];//通知地址
				$subject = $_POST['subject'];//交易标题
				$buyer_email = $_POST['buyer_email'];//买家账号
				$body = $_POST['body'];//交易详细内容
				$show_url = $_POST['show_url'];//商品展示网址
				$default_bank = $_POST['default_bank'];//默认银行
				$return_url = $_POST['return_url'];//跳转地址	
				$royalty_parameters = $_POST['royalty_parameters'];//分润账号	
				$signkey = $_POST['signkey'];//签约密钥


/************************************************************/

//构造要请求的参数数组，无需改动
$parameter = array(
		"service"	=> $service,
		"partner_id"	=> $partner_id,
		"input_charset"	=> $input_charset,
		"sign_type"	=> $sign_type,
		"out_trade_no"	=> $out_trade_no,
		"amount"	=> $amount,
		"payMethod"	=> $payMethod,
		"seller_email"	=> $seller_email,
		"notify_url"	=> $notify_url,
		"subject"	=> $subject,
		"buyer_email"	=> $buyer_email,
		"body"	=> $body,
		"show_url"	=> $show_url,
		"default_bank"	=> $default_bank,
		"return_url"	=> $return_url,
		"royalty_parameters"	=> $royalty_parameters
);

//建立请求
$epaySubmit = new EpaySubmit();
$html_text = $epaySubmit->buildRequestForm($parameter, $signkey,"get", "确认");
echo $html_text;

?>
</body>
</html>