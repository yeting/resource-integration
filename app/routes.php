<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */
Log::debug('route input all',Input::all());
Log::debug('route input header',Input::header());
// Log::debug('route input $_SERVER',$_SERVER);

Route::get('/', function () {
	// Debugbar::info('1111');
	return View::make('hello');
});

Route::post('oauth/access_token', 'OAuthController@postAccessToken');

Route::api(['version' => 'v1','protected' => true], function () {
// Route::api(['version' => 'v1'], function () {

	Route::resource('users', 'UserController');

	Route::resource('orders', 'OrderController');
	Route::resource('cart', 'CartController');
	// Route::resource('stores', 'StoreController');
	// Route::resource('products', 'ProductController');
	// Route::resource('services', 'ServiceController');
});
Route::api(['version' => 'v1'], function () {
	// 关联 席位\服务\需求 到某个分类下
	// /categories/{id}/{type}/{recordId}
	Route::post('categories/{id}/{type}/{recordId}','CategoryController@attach');
	Route::resource('categories', 'CategoryController');

	Route::resource('images', 'ImageController');

	Route::resource('stores', 'StoreController');
	Route::resource('products', 'ProductController');
	Route::resource('services', 'ServiceController');
	Route::resource('demands', 'DemandController');



	Route::resource('metadata', 'MetadataController');
	
	Route::resource('pay', 'PaymentController');
	Route::post('pay/notifyepay','PaymentController@notifyepay');

});