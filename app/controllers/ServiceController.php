<?php


class ServiceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /service
	 *
	 * @return Response
	 */
	public function index() {
		//
		$services = new Service;
		

		if (Input::get('category_id')) {
			$services = $services->whereHas('categorise',function($q){
			    $q->where('category_id', '=', Input::get('category_id'));
			});
		}
		
		if (Input::get('name')) {
			$services = $services->where('name','like', '%'.Input::get('name').'%');
		}

		
		if ($storeID = Input::get('store_id')) {
			$services = $services->where('store_id',$storeID);
		}else{
			$services = $services->with('Store');
		}


		if (Input::get('limit')){
			$services = $services->limit(Input::get('limit'))->get();
		}else{
			$services = $services->paginate(16);
		}
		
		return $services;
		// return '$services';
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /service/create
	 *
	 * @return Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /service
	 *
	 * @return Response
	 */
	public function store() {
		//
		$service = new Service();
		$service->fill(Input::get());
		
		$store = Store::find(Input::get('store_id'));
		$service->store()->associate($store);

		if($service->save()){
			return $service;
		}else{
			throw new Dingo\Api\Exception\StoreResourceFailedException("创建服务信息不成功", $service->errors()->all());
			// return $product->errors()->all();
		}
	}

	/**
	 * Display the specified resource.
	 * GET /service/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
		$service = Service::find($id);
		// TODO:查询不到的时候抛出异常,并遵循错误格式返回错误信息
		if ($service) {
			$service->store;
		}else{
			throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException("没有找到该服务信息");
		}
		// $service->categorise;
		return $service;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /service/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /service/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//修改服务信息
		$service = Service::find($id);
		$service->fill(Input::get());
		if ($service->save()) {
			return $service;
		}else{
			throw new Dingo\Api\Exception\UpdateResourceFailedException("更新服务信息不成功", $service->errors()->all());
		}

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /service/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

		$service = Service::find($id);
		if ($service->delete()) {
			return ['message'=>'删除服务信息成功'];
		}else{
			throw new Dingo\Api\Exception\DeleteResourceFailedException("删除服务信息不成功,请稍后再试");
		}

	}

}