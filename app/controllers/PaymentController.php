<?php

class PaymentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /payment
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		echo PaymentOP::process();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /payment/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * 根据订单号生成支付
	 * POST /payment
	 *
	 * @return Response
	 */
	public function store()
	{
		// 1.验证输入
		$rules = array(
				'order_id' => 'required',
				'method'  => 'required|in:alipay,epay'
		    ); 
		
		$validator = Validator::make(
		    Input::get(),
		    $rules
		);
 		
 		if ($validator->fails())
 		{
 		    throw new Dingo\Api\Exception\StoreResourceFailedException("创建支付失败",$validator->messages());
 		}

 		$order = Order::find(Input::get('order_id'));

 		$payment = new Payment;

 		$payment->order()->associate($order);
 		$payment->createSerialNumber();
 		$payment->payOrder();
 		$payment->changeStatus(0);

 		// 创建支付
 		$payment->method =Input::get('method');

		$payment->save(); 		

		return $payment;
	}

	/**
	 * Display the specified resource.
	 * GET /payment/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /payment/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /payment/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /payment/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function notifyepay()
	{
		// E 支付通知地址,e支付返回通知结果：true（交易处理成功）或false （交易处理失败）
		Log::notice('notifyepay',Input::get());
		// seller_email
		// buyer_email
		// total_fee
		// trade_status 
		return 'true';
	}

	public function returnepay()
	{
		Log::notice('returnepay',Input::get());

		// 1.检验支付状态&&签名结果;
		if (Input::get('is_success') !== 'T') {
			Log::warning('returnepay fail',Input::get());
			return ;
		}else{
			// 2.若成功,修改订单状态
			Input::get('out_trade_no');
			// 3.修改订单已支付金额
			// 4.关闭页面
		}
		
		

	}
}