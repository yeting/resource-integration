<?php

class CartController extends \BaseController {



	/**
	 * Display a listing of the resource.
	 * GET /cart
	 *
	 * @return Response
	 */
	public function index()
	{
		//

		// $cart = Cart::find(1);
		// $cart =new Cart();
		// // $product = $cart->cartable;
		// $product = Product::find(1);
		// // $product = $cart->cartable()->associate($product);
		// // $cart->save();
		// $product->cart()->save($cart);
		// // $cart = $product->cart;
		// // $cart = $product->cart()->save($product);
		// // $cart = $cart->cartable();
		// echo "string";
		// // Log::debug('product cart',$cart);
		// // var_dump($cart);
		// Debugbar::info($product);
		// return $product;

		// 获取当前用户
		$user = $this->auth->user();
		$cart = $user->cart;
		foreach ($cart as &$item) {
			// var_dump($item);
			$item->cartable;
		}
		return $cart;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /cart/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * 向当前用户购物车添加商品
	 * POST /cart
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		// $cart = new Cart();
		// $product = Product::find(20);
		// $product->cart()->associate($cart);
		 if (Input::get('id') =='' || Input::get('type') == '' || Input::get('quantity') == '') {
			throw new Dingo\Api\Exception\StoreResourceFailedException("加入购物车失败");
		 }
		 
		
		$user = $this->auth->user();
		$user_cart = $user->cart->filter(function($cart)
		{
			return $cart->cartable_id == Input::get('id') && $cart->cartable_type == Str::title(Input::get('type')) ;
			// return 0;
		});
		
		$cart = $user_cart->first();
		
		if ($cart) {
			$cart->quantity = $cart->quantity + Input::get('quantity');
			$cart->total();
			$cart->save();
		}else{
			// 判断是否是自己的席位
			// $store = $user->store();
			
			// 1.申明新的购物车
			$cart = new Cart();
			// 2.获取当前用户
			$cart->user()->associate($user);

			// $cart->user_id = $user->id;

			// 3.获取添加商品类型
			$type = Input::get('type');
			$id = Input::get('id');
			switch ($type) {
				case 'service':
					$service = Service::find($id);

					$cart->name = $service->name;
					$cart->price = $service->price;

					$cart->store()->associate($service->store);

					$cart->quantity = Input::get('quantity');

					// 计算该商品在购物车的总价,小计
					$cart->total();

					$cart->cartable()->associate($service);

					$cart->save();
					break;
				default:
					break;
			}
		}


		

		return $cart;

	}

	/**
	 * Display the specified resource.
	 * GET /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /cart/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$cart = Cart::find($id);
		$cart->delete();
	}

}