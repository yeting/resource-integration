<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /product
	 *
	 * @return Response
	 */
	public function index() {
		// 筛选商品列表

		// $parameters['store_id'] =  9962;
		// $parameters['id'] =  1;
		// $products = new Product;
		// foreach ($parameters as $key => $value) {
		// 	$products = $products->where($key,$value);
		// }
		// $products = $products->paginate(16);
		$products = new Product;
		// TODO:根据StoreID筛选
		if ($storeID = Input::get('store_id')) {
			$products = $products->where('store_id',$storeID);
		}else{
			$products = $products->with('Store');
		}

		// TODO:根据categoryID筛选
		// if ($categoryID = Input::get('category_id')) {
		// 	$products = Product::where('category_id',$categoryID)->paginate(16);
		// }
		// TODO:根据父categoryID筛选

		$products = $products->paginate(16);
		// echo "string";
		// else{
		// 	$products = Product::with('Store')->where('store_id',9962)->paginate(16);
		// }


		return $products;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /product/create
	 *
	 * @return Response
	 */
	public function create() {
		//

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /product
	 *
	 * @return Response
	 */
	public function store() {
		// 添加产品信息
		// TODO:只有网站管理员和席位管理员可以添加信息
		$product = new Product();
		// return $product;
		if($product->save()){
			return $product;
		}else{
			throw new Dingo\Api\Exception\StoreResourceFailedException("Could not create new product", $product->errors()->all());
			// return $product->errors()->all();
		}

	}

	/**
	 * Display the specified resource.
	 * GET /product/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
		$product = Product::findOrFail($id);
		// TODO:查询不到的时候抛出异常,并遵循错误格式返回错误信息
		$product->store;
		return $product;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /product/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /product/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
		$product = Product::find($id);
		$product->name = Input::get('name');
		$product->price = Input::get('price');
		$product->description = Input::get('description');

		if ($product->save()) {
			return $product;
		}else{

		}

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /product/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}

}