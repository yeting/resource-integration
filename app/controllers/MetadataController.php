<?php

class MetadataController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /metadata
	 *
	 * @return Response
	 */
	public function index()
	{
		$all = Metadata::all();

		$metadata = Metadata::group($all); 

		
		//
		// $metadata = Metadata::where('module', '=', 'common')->get();

		// $metadataGroup = $metadata->groupBy('section');


		// var_dump($metadataGroup);
		// Log::info('$metadataGroup',$metadataGroup);
		// $metadata->;
		// $metadata = [];
		// $metadata['home'] = [];
		// $sidebar = Metadata::where('module', '=', 'common')->where('section', '=', 'home')->first();
		// $sidebar->value = json_decode($sidebar->value);
		// $metadata['home']['sidebar'] = $sidebar;
		// $sidebar = array_get($metadataGroup,'home.sidebar');
		// array_set($array, 'names.editor', 'Taylor');



		// return $metadataGroup->toArray();
		return $metadata;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /metadata/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /metadata
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /metadata/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /metadata/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /metadata/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /metadata/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}