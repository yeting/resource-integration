<?php

class DemandController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /demand
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$demands = new Demand;

		if (Input::get('category_id')) {
			$demands = $demands->whereHas('categorise',function($q){
			    $q->where('category_id', '=', Input::get('category_id'));
			});
		}

		if ($storeID = Input::get('store_id')) {
			$demands = $demands->where('store_id',$storeID);
		}else{
			$demands = $demands->with('Store');
		}

		$demands = $demands->paginate(16);
		
		return $demands;
		// return '$demands';
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /demand/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /demand
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$demand = new Demand();
		$demand->fill(Input::get());

		$store = Store::find(Input::get('store_id'));
		$demand->store()->associate($store);

		if($demand->save()){
			return $demand;
		}else{
			throw new Dingo\Api\Exception\StoreResourceFailedException("创建需求信息不成功", $demand->errors()->all());
			// return $product->errors()->all();
		}
	}

	/**
	 * Display the specified resource.
	 * GET /demand/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$demand = Demand::find($id);

		if ($demand) {
			$demand->store;
		}
		else{
			throw new Symfony\Component\HttpKernel\Exception\NotFoundHttpException("没有找到该需求信息");
		}

		return $demand;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /demand/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /demand/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$demand = Demand::find($id);
		$demand->fill(Input::get());
		if ($demand->save()) {
			return $demand;
		}else{
			throw new Dingo\Api\Exception\UpdateResourceFailedException("更新服务信息不成功", $demand->errors()->all());
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /demand/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// echo '111';
		$demand = Demand::find($id);

		if ($demand->delete()) {
			return ['message'=>'删除需求信息成功'];
		}else{
			throw new Dingo\Api\Exception\DeleteResourceFailedException("删除需求信息不成功,请稍后再试");
		}
	}

}