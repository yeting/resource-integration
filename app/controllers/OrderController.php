<?php

class OrderController extends \BaseController {
	public function __construct() {
		// $this->protect('index');

		// You can also pass an array or a pipe separated string.
		// $this->protect(['index', 'posts']);
		// $this->protect('index|posts');

		// Do not pass in any method name to protect all methods.
		$this->protect();

		// The same rules apply to the "unprotect" method.
	}

	/**
	 * 获取当前用户订单,需要认证
	 * GET /order
	 *
	 * @return Pagination
	 */
	public function index() {
		//当前用户信息,筛选用户订单
		
		$user   = $this->auth->user();

		// $orders = Order::paginate();
		if (Input::get('store_id')) {
			$orders = new Order;
			$orders = $orders->where('store_id','=',Input::get('store_id'))->paginate(15);
		}
		else
		{
			$orders = $user->orders()->with('Store')->orderBy('id', 'desc')->paginate(10);
			foreach ($orders as $key => &$value) {
				$value->items;
			};
		}

		return $orders;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /order/create
	 *
	 * @return Response
	 */
	public function create() {
		

	}

	/**
	 * Store a newly create d resource in storage.
	 * POST /order
	 *
	 * @return Response
	 */
	public function store() {
		
		// 验证输入
		$rules = array(
				'contact' => 'required',
				'mobile'  => 'required',
				'address' => 'required',
				'cart'    => 'required|array|min:1',
		    ); 
		

		$validator = Validator::make(
		    Input::get(),
		    $rules
		);
 		
 		if ($validator->fails())
 		{
 		    throw new Dingo\Api\Exception\StoreResourceFailedException("创建订单失败",$validator->messages());
 		}
		

		// 创建订单,根据从购物车编号创建订单
		$user = $this->auth->user();

		$cart = $user->cart->filter(function($cart)
		{
			return in_array($cart->id,Input::get('cart'));
			// return 0;
		});

		if ($cart->isEmpty()) {
			throw new Dingo\Api\Exception\StoreResourceFailedException("创建订单失败",[ 'cart' =>'购物车商品不存在']);
		}

		$storeGroup = $cart->groupBy('store_id');

		foreach ($storeGroup as $key => $store) {
			$order = new Order;
			$order->user()->associate($user);
			$order->store_id = $key;

			$order->changeStatus(0);
			$order->createNumber();

			// 填充联系人.联系电话.收货地址
			$order->fill(Input::get());

			$items = [];

			while ( $cart = array_shift($store)) {
				$item = new OrderItem();

				$item->fillFormCart($cart);

				array_push($items, $item);
			}
			$order->save();
			$order->items()->saveMany($items);

			$order->items_total();
			$order->total();
			$order->save();
		}

		return ['message'=>'创建订单成功'];
		// return $this->response->created();

		// return ['message'=>'创建订单成功'];
		// while ($store = $storeGroup->shift()) {

		// 	// return $store;
			

		// }


		// $storeGroup->each(function($store){
		// 	$order = new Order;
		// 	$order->user()->associate($user);
			
		// 	$items = [];
		// 	while ( $cart = $store->shift() ) {
		// 		$item = new OrderItem($cart->toArray());
		// 		array_push($items, $item);
		// 	}

		// 	$order->items()->saveMany($items);

		// });
		// return $storeGroup;
	}

	/**
	 * Display the specified resource.
	 * GET /order/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		// 获取订单信息
		if (strlen($id) == 16) {
			$order = Order::where('number','=',$id)->first();

		}else{
			$order = Order::find($id);
		}
		$order->items;
		return $order;

	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /order/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		// 
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /order/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /order/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
		$order = Order::find($id);
		if ($order->delete()) {
			return ['message'=>'删除订单信息成功'];
		}else{
			throw new Dingo\Api\Exception\DeleteResourceFailedException("删除订单信息不成功,请稍后再试");
		}

	}

}