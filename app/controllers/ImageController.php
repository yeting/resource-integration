<?php

class ImageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /image
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /image/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /image
	 *
	 * @return Response
	 */
	public function store()
	{

		// return Input::all();

		//保存图片
		$rules = array(
			'image'=> 'required|image'
		);

		$validation = Validator::make(Input::all(),	$rules);

		if($validation->fails()) {
			throw new Dingo\Api\Exception\StoreResourceFailedException('不是规定的图片格式或者图片大小超过限制',$validation->messages());
		}


		//If the validation passes, we upload the image to the database and process it
		$image = Input::file('image');
		
		//This is the original uploaded client name of the image
		$filename = $image->getClientOriginalName();
		
		//Because Symfony API does not provide filename
		//without extension, we will be using raw PHP here 	Building an Image Sharing Website[ 50  ]
		$filename = pathinfo($filename, PATHINFO_FILENAME);
		
		//We should salt and make an url-friendly version of
		//the filename
		//(In ideal application, you should check the filename
		//to be unique)
		$fullname = Str::slug(Str::random(8).$filename).'.'.$image->getClientOriginalExtension();
		
		//We upload the image first to the upload folder, then 	get make a thumbnail from the uploaded image
		$upload = $image->move(Config::get( 'image.upload_folder_base'),$fullname);
		
		//Our model that we've created is named Photo, this library has an alias named Image, don't mix them two!
		//These parameters are related to the image processing class that we've included, not really related to Laravel
		
		// ImageOP::make(Config::get( 'image.upload_folder').'/'.$fullname)
		// 	->resize(Config::get( 'image.thumb_width'),null, true)
		// 	->save(Config::get( 'image.thumb_folder').'/'.$fullname);
		
		//If the file is now uploaded, we show an error message to the user, else we add a new column to the database and show the success message
		if($upload) {
			//image is now uploaded, we first need to add column to the database
			// $insert_id = DB::table('photos')->insertGetId(
			// 	array(
			// 	'title' => Input::get('title'),
			// 	'image' => $fullname
			// 	)
			// );
			$imageModel  = Image::create([ 'path'=>$fullname ]);
			$imageModel->path = Config::get( 'image.upload_folder').'/'.$fullname;
			return $imageModel;
			//Now we redirect to the image's permalink
			// return Redirect::to(URL::to('snatch/'.$insert_id))->with('success','Your image is uploaded successfully!');
		} else {
        	throw new Dingo\Api\Exception\StoreResourceFailedException('上传图片不成功,请稍后再试');
			//image cannot be uploaded
			// return Redirect::to('/')->withInput()->with('error','Sorry, the image could not be uploaded, please try again later');
		}
	}

	/**
	 * Display the specified resource.
	 * GET /image/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /image/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /image/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /image/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}