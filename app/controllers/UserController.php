<?php

class UserController extends \BaseController {

	public function __construct()
	{
	    // $this->protect('index');

	    // You can also pass an array or a pipe separated string.
	    // $this->protect(['index', 'posts']);
	    // $this->protect('index|posts');

	    // Do not pass in any method name to protect all methods.
	    // $this->protect();
	    
	    // The same rules apply to the "unprotect" method.
		$this->unprotect(['store']);
	}

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index() {
		// //查看用户列表

		$user = $this->auth->user();
		// var_dump($user);
		// return $user;
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create() {
		
		// $user = $this->auth->user();
		// if ($user->info == null) {
		// 	$user->info()->create([]);
		// }
		// return $user;

		// Debugbar::info($user->info);
		// Excel::selectSheets('sheet1')->load();
		// Excel::selectSheets('Sheet1')->load('app/database/seeds/InitUser.xls', function($reader) {
		// // Excel::load('app/database/seeds/InitUser.csv', function($reader) {
		// 	// $reader->calculate(false);
		//     // Getting all results
		//     // $results = $reader->get();

		//     // ->all() is a wrapper for ->get() and will work the same
		//     // $results = $reader->select(array('username', 'password','store_no'))->dd();
		// 	// $reader->limit(15)->dd();
		// 	// Loop through all sheets
		// 	// $reader->each(function($sheet) {

		// 	//     // Loop through all rows
		// 	//     $sheet->each(function($row) {
		// 	//     	dd($row);
		// 	//     });

		// 	// });
		// 	// dd($reader->limit(15)->toArray());
		// 	echo "string";
		// 	$reader->each(function($row) {
		// 		Debugbar::info('000'.$row->username);
		// 		Debugbar::info($row);
				
		//         // Loop through all rows
		//         // $sheet->each(function($row) {
		//         // 	Debugbar::info($row);
		//         // });

		//     });
		// });
		
		// $group = Sentry::createGroup(array(
		// 	'name'        => 'Admin',
		// 	'permissions' => array(
		// 		'admin' => 1,
		// 		'users' => 1,
		// 	),
		// ));

		// $user = Sentry::createUser(array(
		// 	'email'     => 'john.doe@example.com',
		// 	'password'  => 'test',
		// 	'activated' => true,
		// ));
		// // Find the group using the group id
		// $adminGroup = Sentry::findGroupById(1);

		// // Assign the group to the user
		// $user->addGroup($adminGroup);


		// $group = Sentry::createGroup(array(
		// 	'name'        => 'Admin',
		// 	'permissions' => array(
		// 		'admin' => 1,
		// 		'users' => 1,
		// 	),
		// ));

		// $user = Sentry::createUser(array(
		// 	'username'  => 'admin',
		// 	'password'  => 'admin',
		// 	'activated' => true,
		// ));
		// // Find the group using the group id
		// $adminGroup = Sentry::findGroupById(1);

		// // Assign the group to the user
		// $user->addGroup($adminGroup);

		// $user = Sentry::createUser(array(
		// 	'username'     => 'test',
		// 	'password'  => 'test',
		// 	'activated' => true,
		// ));


		// // 导入席卡用户
		// Excel::selectSheets('Sheet1')->load('app/database/seeds/InitUser.xls', function($reader) {
		//     $reader->each(function($row) {

		//     	$user = Sentry::createUser(array(
		// 			'username'  => '000'.$row->username,
		// 			'password'  => $row->password,
		// 			'activated' => true,
		//     	));

		//     	$store = Store::create([
		// 			'store_no' => $row->store_no,
		//     	]);

		//     	$store->users()->attach($user->id);
		//     });

		// });
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 * json { username:'ling',password:'11223344a'}
	 * 
	 * @return Response
	 */
	public function store() {
		//用户注册
		try
		{
		    // Let's register a user.
		    // var_dump(Input::all());
		    
		    $user = Sentry::register(array(
		        'username' => Input::get('username'),
		        'password' => Input::get('password'),
		    ));

		    // Let's get the activation code
		    $activationCode = $user->getActivationCode();

		    // Send activation code to the user so he can activate the account
		    // TODO:用户激活应为邮件激活
		    $user->attemptActivation($activationCode);
		    
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    // echo 'Login field is required.';
		    throw new Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException('用户名不能为空.');
		    
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    // echo 'Password field is required.';
		    throw new Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException('密码不能为空.');
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    // echo 'User with this login already exists.';
			// throw new Symfony\Component\HttpKernel\Exception\ConflictHttpException('User was updated prior to your request.');	
			throw new Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException('该用户名已经被注册.');
           
		}

	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		// 获取当前用户
		if ($id == 'my') {
			$user = $this->auth->user();
			// 不存在就创建
			if ($user->info == null) {
				$user->info()->create([]);
			}
			$user->store = $user->stores()->first();;

			return $user;
		}
		if ($id == 'store') {
			$user = $this->auth->user();
			$user->store = $user->stores()->first();
			// TODO:没有找到席位的时间需要抛出异常
			if (!$user->store) {
				throw new Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException("access denied");
				
			}
			// $user->stores;
			// echo "string";
			return $user;
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		// 修改当前当前用户信息
		if ($id == 'my') {
            
            $user = $this->auth->user();
			// var_dump($user->attributesToArray());
            // $user = new User();
			$user->fill(Input::get());
			// $user->save();
			$user->info->fill(Input::get('info'));
			// $user->info->save();
			$user->push();

			if(Input::get('store')) {
				$user->store = $user->stores()->first();
				if ($user->store) {
					$user->store->fill(Input::get('store'));
					$user->store->save();
				}
			}

			return $user;
            // var_dump($user->attributesToArray());
			//return $user;
			// $user = User::newFromBuilder(Input::all());
			

			// $userinfo = $user->info;
			// $info = Input::get('info');
			// $userinfo->address = $info['address'];
			// $userinfo->birthday = $info['birthday'];
			// $userinfo->company = $info['company'];
			// $userinfo->company_address = $info['company_address'];
			// $userinfo->gender = $info['gender'];
			// $userinfo->mobile = $info['mobile'];
			// $userinfo->position = $info['position'];
			// if($userinfo->save()){
			// 	return $user;
			// }

			// $userinfo->position = '11';
			// $res = $userinfo->save(Input::get('info'));

			// $log['return'] = $res;
			
			// Log::debug('Usercontroller update',$log);
			
			// Log::debug('Usercontroller update',$user->info->toArray());

			// Log::debug('Usercontroller $userinfo',$userinfo->toArray());
			
			
			// var_dump(Input::get('user'));
			// var_dump(Input::all());
			// $userinfo = new UserInfo(Input::get('info'));
			// $user->info()->save($userinfo);

			// return $user;
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}

}