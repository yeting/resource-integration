<?php

class StoreController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /store
	 *
	 * @return Response
	 */
	public function index() {
		//筛选席位
		$stores = new Store;
		// $stores = Store::where('name' ,'!=' ,'')->paginate(16);
		if (Input::get('category_id')) {
			$stores = $stores->whereHas('categorise',function($q){
			    $q->where('category_id', '=', Input::get('category_id'));
			});
		}

		if (Input::get('name')) {
			$stores = $stores->where('name','like', '%'.Input::get('name').'%');
		}
		
		$stores = $stores->where('name' ,'!=' ,'');
		
		if(Input::get('limit')){
			$stores = $stores->limit(Input::get('limit'))->get();
			// var_dump($stores->toArray());
		}else{
			$stores = $stores->paginate(16);
		}

		return $stores;
		// return '1';
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /store/create
	 *
	 * @return Response
	 */
	public function create() {
		//
		// echo "string";

		// $store = Store::create([
		// 	'name'        => '111111',
		// 	'description' => '2222222',
		// 	'images'      => '333333333',
		// 	'company'     => '44444444',
		// ]);

		// // $store = Store::find($store->id);
		// $product = new Product([
		// 	'name'         => '22222',
		// 	'description'  => '22222',
		// 	'images'       => '22222',
		// 	'price'        => '22222',
		// 	'available_on' => '22222',
		// ]);
		// $store->products()->save($product);

		// Debugbar::info($store->products);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /store
	 *
	 * @return Response
	 */
	public function store() {
		//
	}

	/**
	 * Display the specified resource.
	 * GET /store/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
		$store = Store::find($id);
		$user = $store->users()->first();
		if ($user) {
			$user->info;
			$store->user = ['name'=>$user->last_name,'mobile'=>$user->info->mobile,'qq'=>$user->info->qq];
		}
		return $store;
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /store/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /store/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		//
		$store = Store::find($id);
		// $store = new Store();
		// $store->name = Input::get('name');
		// $store->company = Input::get('company');
		// $store->description = Input::get('description');
		$store->fill(Input::get());
		// $store->save();
		if ($store->save()) {
			return $store;
		}else{
			throw new Dingo\Api\Exception\UpdateResourceFailedException("更新席位信息不成功", $service->errors()->all());
		}
		// return $store;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /store/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}

}