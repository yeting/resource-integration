<?php

class CategoryController extends \BaseController {

	/**
     * Display a listing of the resource.
     * GET /category
     *
     * @return Response
     */
	public function index()
	{
        $rules = array(
                'type'  => 'required|in:store,service'
            ); 
        
        $validator = Validator::make(
            Input::get(),
            $rules
        );
        
        if ($validator->fails())
        {
            throw new Symfony\Component\HttpKernel\Exception\BadRequestHttpException("参数错误");
        }


        $categories = new Category;

        $categories = $categories->where('type', '=' , Str::title(Input::get('type')));

        $categories = $categories->get();
        return $categories;        
        // return '$categories';        
		//
        // tmp
        // $stores = $this->api->get('stores', ['limit' => '10']);
        // var_dump($sotres);
        // return $sotres;
        // for ($i=1; $i < 6; $i++) { 
            // foreach ($stores as $key => $value) {
            //     // echo "categories/$i/store/{$value['id']}";
            //     $this->api->post( "categories/6/store/{$value['id']}");
            // }
        // }
       
	}

	/**
     * Show the form for creating a new resource.
     * GET /category/create
     *
     * @return Response
     */
	public function create()
	{
		//
	}

	/**
     * Store a newly created resource in storage.
     * POST /category
     *
     * @return Response
     */
	public function store()
	{
		// 添加分类信息
		$category = new Category();

		if($category->save()){
			return $category;
		}else{
			throw new Dingo\Api\Exception\StoreResourceFailedException("创建分类失败", $category->errors()->all());
		}
		
		// $category = Category::find(1);
		// $product = Product::find(1);

		// $product->categorise()->attach($category);
		// return $product;
	}

	/**
     * Display the specified resource.
     * GET /category/{id}
     *
     * @param  int  $id
     * @return Response
     */
	public function show($id)
	{
		//
	}

	/**
     * Show the form for editing the specified resource.
     * GET /category/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
	public function edit($id)
	{
		//
	}

	/**
     * Update the specified resource in storage.
     * PUT /category/{id}
     *
     * @param  int  $id
     * @return Response
     */
	public function update($id)
	{
		//
	}

	
    
	/**
     * 关联 席位\服务\需求 到某个分类下
     * POST /categories/{id}/{type}/{recordId}   
     * 
     * @param int       $id 
     * @param string    $type       store|service|demand
     * @param int       $recordId 
     */ 
	public function attach($id, $type, $recordId)
	{
        
        switch ($type) { 
        	
            case 'store':
                $res = Store::find($recordId);
                break;
            case 'service':
                $res = Service::find($recordId);
                break;
            case 'demand':
                $res = Demand::find($recordId);
                break;
            default:
                throw new Dingo\Api\Exception\StoreResourceFailedException('不可识别的分类类型');
                break;
        }
        $category = Category::find($id);

        if ($res&&$category) {
        	$res->categorise()->attach($category);
        	return $res;
        }
        else{
        	throw new Dingo\Api\Exception\StoreResourceFailedException('资源不存在');
        }
        
        
        

		// $product->categorise()->attach($category);
		// return $product;
	}

	/**
     * Remove the specified resource from storage.
     * DELETE /category/{id}
     *
     * @param  int  $id
     * @return Response
     */
	public function destroy($id)
	{
		//
	}

}