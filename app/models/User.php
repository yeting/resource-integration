<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = array('last_name');
	protected $hidden = array('password', 'remember_token','persist_code','reset_password_code','activated','activation_code');


	public function info()
    {
    	
        return $this->hasOne('UserInfo');
    }

    public function stores()
    {
        return $this->belongsToMany('Store','users_stores');
    }
    public function store()
    {
        return $this->stores()->first();
    }

    public function cart()
    {
    	return $this->hasMany('Cart');
    }

    public function orders()
    {
        return $this->hasMany('Order');
    }
}
