<?php


use LaravelBook\Ardent\Ardent;

class Product extends Ardent {
	
	public $autoHydrateEntityFromInput = true; 
	// public $forceEntityHydrationFromInput = true;

	public static $rules = array(
		'name'        => 'required|between:4,16',
		'description' => 'required',
		'price'       => 'required|digits_between:1,10',
		'store_id'    => 'required|digits_between:1,10',
    );

    public static $customMessages = array(
        'required' => ' :attribute 是必填的.',
        'between'  => ' :attribute 长度必须在:min - :max之间.',
        'digits_between'   => ' :attribute 必须是数字,长度必须在:min - :max之间.',
    );
	
	protected $guarded = array('id');

	public function store() {
		return $this->belongsTo('Store');
	}

	public function categorise(){
		return $this->morphToMany('Category', 'categoryable');
	}

	public function cart()
    {
        return $this->morphMany('Cart', 'cartable');
    }
}