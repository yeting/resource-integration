<?php
use LaravelBook\Ardent\Ardent;
// class Service extends \Eloquent {
class Service extends Ardent {

	// public $autoHydrateEntityFromInput = true; 
	// public $forceEntityHydrationFromInput = true;

	public static $rules = array(
		'name'        => 'required|between:4,50',
		'description' => 'required',
		'price'       => 'required|digits_between:1,10',
		'store_id'    => 'required|digits_between:1,10',
    );

    public static $customMessages = array(
        'required' => ' :attribute 是必填的.',
        'between'  => ' :attribute 长度必须在:min - :max之间.',
        'digits'   => ' :attribute 必须是数字.',
    );
	
	/**
	 * 软删除
	 */
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description','price','images'];
    protected $hidden = [];
	// protected $guarded = array('id');

	public function store() {
		return $this->belongsTo('Store');
	}

	public function categorise(){
		return $this->morphToMany('Category', 'categoryable');
	}
}