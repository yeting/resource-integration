<?php

class Metadata extends \Eloquent {
	protected $fillable = [];

	protected $table = 'metadata';


	public static function group($all)
	{
		$results = [];

		foreach ($all as $key => $value) {
			$value['value'] = is_null(json_decode($value['value']))?$value['value']:json_decode($value['value']);
			// $value['value'] = json_decode($value['value']);
			$results[$value['module']][$value['section']][$value['key']] = $value;
		}

		return $results;
	}
}