<?php

class OrderItem extends \Eloquent {
	protected $fillable = ['quantity'];

	public function order()
	{
		return $this->belongsTo('Order');
	}


	public function itemable()
	{
		return $this->morphTo();
	}

	public function fillFormCart(Cart $cart){

		$this->name = $cart->name;
		$this->unit_price = $cart->price;
		$this->quantity = $cart->quantity;
		
		$this->itemable_id = $cart->cartable_id;
		$this->itemable_type = $cart->cartable_type;

		$this->total();
	}

	public function total(){
		$this->total = $this->unit_price * $this->quantity;
	}
}