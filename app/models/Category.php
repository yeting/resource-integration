<?php

use LaravelBook\Ardent\Ardent;

class Category extends Ardent {

    public $autoHydrateEntityFromInput = true; 

    public static $customMessages = array(
        'required' => ' :attribute 是必填的.',
        'between'  => ' :attribute 长度必须在:min - :max之间.',
      );

    public static $rules = array(
        'name'        => 'required',
        'type'        => 'required',
    ); 

    // protected $guarded = array('id');
	protected $fillable = ['name','type','description'];

    public function stores()
    {
        return $this->morphedByMany('Store', 'categoryable');
    }

    public function products()
    {
        return $this->morphToMany('Product', 'categoryable');
    }

    public function services()
    {
        return $this->morphedByMany('Service', 'categoryable');
    }

    public function demands()
    {
        return $this->morphedByMany('Demand', 'categoryable');
    }
}