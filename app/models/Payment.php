<?php

class Payment extends \Eloquent {
	protected $fillable = [];

	protected $table = 'payment';

	public function order()
    {
        return $this->belongsTo('Order');
    }

    public function payOrder($amount = 'all')
    {
    	$order = $this->order;
    	if ($amount == 'all') {
    		$this->amount = $order->total;
    	}else{
    		$this->amount = $amount;
    	}
    }

    public static $statusList = [ '0'=>'create','1'=>'paid','2'=>'finished',
    								'3'=>'closed','4'=>'fail' ];
     
    public function changeStatus($statusCode)
    {
		return $this->status = static::$statusList[$statusCode];
    }

    public function createSerialNumber()
    {
    	if ($this->serial_number) {
    		return $this->serial_number;
    	}
    	return $this->serial_number = date('ymd').substr(time(),-5).substr(microtime(),2,5); 
    }
}