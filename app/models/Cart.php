<?php


class Cart extends \Eloquent {


	protected $fillable = [];

    protected $table = 'cart';
    
    public function cartable()
    {
        return $this->morphTo();
    }

    public function store()
    {
        return $this->belongsTo('Store');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }


	public function total(){
		return $this->total = $this->price * $this->quantity;
	}
}