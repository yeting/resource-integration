<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Order extends \Eloquent {

	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	protected $fillable = ['contact','mobile','address'];


	public function user()
    {
        return $this->belongsTo('User');
    }

    public function store()
    {
        return $this->belongsTo('Store');
    }

    public function items()
    {
    	return $this->hasMany('OrderItem');
    }

    public function payments()
    {
        return $this->hasMany('Payment');
    }

    public function changeStatus($statusCode)
    {
    	// array('pending', 'confirmed', 'shipped', 'abandoned', 'cancelled', 'returned');
    	$statusList = [ '0'=>'pending','1'=>'confirmed','2'=>'shipped',
    					'3'=>'succeed','4'=>'cancelled','5'=>'returned' ];

    	return $this->status = $statusList[$statusCode];
    }

    public function createNumber()
    {
    	if ($this->number) {
    		return $this->number;
    	}
    	return $this->number = date('ymd').substr(time(),-5).substr(microtime(),2,5); 
    }

    public function items_total($recalculate = false)
    {

    	if (!$this->items_total || $recalculate) {
    		$items_total = 0;

    		foreach ($this->items as $key => $value) {
    			$items_total = $items_total + $value->total;
    			Log::debug('items_total:'.$items_total);
    		}

    		$this->items_total = $items_total;
    	}
    	

		return $this->items_total;
	}

	public function total()
	{
		return $this->total = $this->items_total();
	}

}