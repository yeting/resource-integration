<?php

use LaravelBook\Ardent\Ardent;


class Store extends Ardent {

	public $autoHydrateEntityFromInput = true; 
	public $autoPurgeRedundantAttributes = true;

	public static $rules = array(
		'name'        => 'required|between:4,16',
		'description' => 'required',
    ); 

	protected $fillable = ['name','description','brief','images','company','business_license_img','special_license_img'];

	// protected $guarded = array('id');

	public function products() {
		return $this->hasMany('Product');
	}

	public function services() {
		return $this->hasMany('Service');
	}

	public function demands() {
		return $this->hasMany('Demand');
	}


	// 席位分类
	public function categorise(){
		return $this->morphToMany('Category', 'categoryable');
	}


	public function orders(){
        return $this->hasMany('Order');
	}

	/**
	 * 席位所属用户
	 * @return [Eloquent/User] [用户模型]
	 */
	public function users()
    {
        return $this->belongsToMany('User','users_stores');
    }

}