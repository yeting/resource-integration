<?php

// use LaravelBook\Ardent\Ardent;

class UserInfo extends \Eloquent {
	

	// public static $rules = array(
	// 	'gender'   => 'required|between:4,16',
	// 	'birthday' => 'required|email',
	// 	'mobile'   => 'required|alpha_num|between:4,8|confirmed',
	// 	'address'  => 'required|alpha_num|between:4,8',
 //  	);
	// protected $fillable = [];

	// protected $guarded = array('id');
	protected $fillable = ['gender','birthday','mobile','phone','address','company','position','company_address','idcard_img','idcard_no','phone','qq'];

	protected $table = 'user_info';
	
	public function user()
    {
        return $this->belongsTo('User');
    }
}