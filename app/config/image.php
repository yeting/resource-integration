<?php
/**
* app/config/image.php
*/
return array(
	//the folder that will hold original uploaded images
	'upload_folder' => 'http://juyuannet.com:8000/uploads',
	'upload_folder_base' => 'uploads',
	//the folder that will hold thumbnails
	'thumb_folder' => 'uploads/thumbs',
	'thumb_folder_base' => 'uploads/thumbs',
	//width of the resized thumbnail
	'thumb_width' => 320,
	//height of the resized thumbnail
	'thumb_height' => 240
);
