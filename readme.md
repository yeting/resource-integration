## Change Logs

### 2015/1/8

企业名 		user_info.company
地址 		user_info.company_address
手机 		user_info.mobile
交易员姓名 	user.last_name

电话 			info.phone
交易员复印号 	info.idcard_no
交易员复印件 	info.idcard_img

营业执照复印件  store.business_license_img
特殊行业许可证 	store.special_license_img
企业简介		store.description

